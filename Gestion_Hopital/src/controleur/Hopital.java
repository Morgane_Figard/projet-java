/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controleur;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import vue.*;
import modele.*;

/**
 *
 * @author Morgane
 */
public class Hopital {
    
    private static MenuPrincipal fenetre;

    public void recherche() throws SQLException, ClassNotFoundException {
        
        Recherche fen_recherche = new Recherche();
        //fen_recherche.setFields();
        fen_recherche.setVisible(true);
    }
    
    /**
     *
     * @throws SQLException
     */
    public void maj() throws SQLException {
        
        try {
            Maj fen_maj = new Maj();
            fen_maj.setVisible(true);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Hopital.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void reporting() throws SQLException, ClassNotFoundException {
        
        Reporting fen_reporting = new Reporting();
        fen_reporting.setVisible(true);
    }
    
    /**
     * @param args the command line arguments
     * @throws java.sql.SQLException
     * @throws java.lang.ClassNotFoundException
     */
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        
        fenetre = new MenuPrincipal();
        fenetre.setVisible(true);
                
    }
}
