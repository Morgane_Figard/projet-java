/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.font.TextAttribute;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import modele.*;

/**
 *
 * @author Morgane
 */
public class Recherche extends JFrame implements ActionListener{
    
    private final GestionBDD gestionnaire;
    JPanel panel;
    JPanel r_par;
    JLabel l_r;
    JComboBox<String> choix_recherche;
    JPanel sous_recherche;
    JLabel l_sr;
    JPanel filtre;
    JLabel l_filtre;
    Vector<JCheckBox> filtres;
    JButton valider;
    JPanel r_av;
    JLabel l_rav;
    JComboBox<String> choix_sous_recherche;
    String selected_recherche;
    String requete;
    JLabel error;
    
    public Recherche() throws SQLException, ClassNotFoundException {
        gestionnaire = new GestionBDD();
        setSize(650,350);
        try{
            setIconImage(ImageIO.read(new File("search-icon.png")));
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        //pan = new JPanel();
        setTitle("Recherche d'informations");
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setTitle("Recherche d'informations");
        setSize(650,350);
        try{
            setIconImage(ImageIO.read(new File("search-icon.png")));
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        r_par = new JPanel();
        r_par.setLayout(new FlowLayout());
        
        l_r = new JLabel();
        l_r.setText("Rechercher un(e)...");
        l_r.setFont(new Font("Arial", Font.PLAIN, 18));
        
        r_par.add(l_r);
        /*GestionBDD gestion = new GestionBDD();
        ArrayList<String> a_table_names = gestion.resultatRequete("SHOW TABLES");
        String[] choix_r = a_table_names.toArray(new String[a_table_names.size()]);*/
        String[] choix_r = new String[] {"service", "chambre", "docteur", "infirmier", "malade", "employé"};
        choix_recherche = new JComboBox(choix_r);
        choix_recherche.setFont(new Font("Arial", Font.PLAIN, 18));
        choix_recherche.addActionListener(this);
        selected_recherche = new String("");
        
        r_par.add(choix_recherche);
        
        sous_recherche = new JPanel();
        sous_recherche.setLayout(new FlowLayout());
        l_sr = new JLabel();
        l_sr.setFont(new Font("Arial", Font.PLAIN, 18));
        valider = new JButton("Valider");
        valider.setFont(new Font("Arial", Font.PLAIN, 18));
        valider.addActionListener(this);
        choix_sous_recherche = new JComboBox();
        choix_sous_recherche.addActionListener(this);
        choix_sous_recherche.setFont(new Font("Arial", Font.PLAIN, 18));
        sous_recherche.add(l_sr);
        
        filtre = new JPanel();
        filtre.setLayout(new FlowLayout());
        l_filtre = new JLabel();
        l_filtre.setFont(new Font("Arial", Font.PLAIN, 18));
        filtre.add(l_filtre);
        
        r_av = new JPanel();
        r_av.setLayout(new FlowLayout());
        l_rav = new JLabel("Recherche avancée...");
        l_rav.setFont(new Font("Arial", Font.PLAIN, 16));
        l_rav.addMouseListener(new MouseAdapter() {
            public void mouseExited(MouseEvent e) {
                if(e.getSource()==l_rav) {
                    l_rav.setFont(new Font("Arial", Font.PLAIN, 16));
                    l_rav.setForeground(Color.black);
                }
            }

            public void mouseEntered(MouseEvent e) {
                if(e.getSource()==l_rav) {
                    l_rav.setFont(new Font("Arial", Font.PLAIN, 20));
                    l_rav.setForeground(Color.magenta.darker());
                    Map attributes = e.getComponent().getFont().getAttributes();
                    attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
                    l_rav.setFont(e.getComponent().getFont().deriveFont(attributes));
                }
            }
            
            public void mouseClicked(MouseEvent e) {
                if(e.getSource()==l_rav) {
                    RechercheAvancee rech_av = new RechercheAvancee();
                    rech_av.setVisible(true);
                }
            }
        });
        r_av.add(l_rav);
                
        panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        //panel.setLayout(new FlowLayout());
        GridBagConstraints c = new GridBagConstraints();
        
        c.gridwidth = 1;    //largeur = une seule colonne
        c.weightx = .01;
        c.weighty = .2;
        c.gridx = 0;    //première colonne
        c.gridy = 0;    //première ligne
        c.ipady = 20;
        c.ipadx = 10;
        c.insets = new Insets(40, 10, 50, 0);
        
        panel.add(r_par, c);
        
        c.gridwidth = 1;    //largeur = une seule colonne
        c.weightx = .01;
        c.weighty = .2;
        c.gridx = 0;    //première colonne
        c.gridy = 1;    //deuxième ligne
        c.ipady = 20;
        c.ipadx = 10;
        c.insets = new Insets(0, 10, 0, 0);
        
        panel.add(sous_recherche, c);
        
        c.gridwidth = 1;    //largeur = une seule colonne
        c.weightx = .01;
        c.weighty = .2;
        c.gridx = 0;    //première colonne
        c.gridy = 2;    //troisième ligne
        c.ipady = 20;
        c.ipadx = 10;
        c.insets = new Insets(0, 10, 0, 0);
        
        panel.add(filtre, c);
        
        c.gridwidth = 1;    //largeur = une seule colonne
        c.weightx = .01;
        c.weighty = .2;
        c.gridx = 0;    //première colonne
        c.gridy = 3;    //quatrieme ligne
        c.ipady = 20;
        c.ipadx = 10;
        c.insets = new Insets(0, 10, 0, 0);
        
        panel.add(r_av, c);
        
        getContentPane().add(panel);
        
        requete = new String("");
        filtres = new Vector();
    }
    
    public void setFields()
    {
        JPanel pan = new JPanel();
        JTextField instructionsRecherche;
        JTextArea entreeUtilisateur;
        JButton validerA;
        pan.setLayout(new BorderLayout());
        instructionsRecherche = new JTextField();
        entreeUtilisateur = new JTextArea();
        validerA = new JButton("Valider");
        instructionsRecherche.setText("Quelle information voulez-vous?");
        entreeUtilisateur.setText("");
        
        pan.add(instructionsRecherche,"North");
        pan.add(entreeUtilisateur);
        pan.add(validerA,"South");
        
        validerA.addActionListener(this);
        entreeUtilisateur.setAlignmentY(TOP_ALIGNMENT);
        instructionsRecherche.setEditable(false);
        entreeUtilisateur.setEditable(true);
        
        entreeUtilisateur.setLineWrap(true);
        entreeUtilisateur.setWrapStyleWord(true);
        
        getContentPane().add(pan);
    }
    
    public void r_par_service() {
        choix_sous_recherche.removeAllItems();
        l_sr.setText("Selectionnez un service :      ");
        choix_sous_recherche.addItem("Reanimation et Traumatologie");
        choix_sous_recherche.addItem("Chirurgie generale");
        choix_sous_recherche.addItem("Cardiologie");
        sous_recherche.add(choix_sous_recherche);
        sous_recherche.add(valider);
    }
    
    public void r_chambre() {
        choix_sous_recherche.removeAllItems();
        filtre.removeAll();
        filtres.removeAllElements();
        l_sr.setText("Trier les chambres par...");
        l_filtre.setText("Filtres : ");
        filtre.add(l_filtre);
                
        String req = "SELECT * FROM chambre";
        
        try {
            gestionnaire.initializeStringV2();
        } catch (SQLException ex) {
        Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        gestionnaire.setHumanRequest(req);
        
        try {
            ArrayList<String> colonne = gestionnaire.getNomsColonne(req);
            
            for(int i=0; i<colonne.size(); i++) {
                choix_sous_recherche.addItem(colonne.get(i));
                filtres.add(new JCheckBox(colonne.get(i)));
                filtres.elementAt(i).setSelected(true);
                filtres.elementAt(i).setFont(new Font("Arial", Font.PLAIN, 18));
                filtre.add(filtres.elementAt(i));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        sous_recherche.add(choix_sous_recherche);
        sous_recherche.add(valider);
    }
    
    public void r_docteur() {
        choix_sous_recherche.removeAllItems();
        l_sr.setText("Trier les docteurs par...");
        filtre.removeAll();
        filtres.removeAllElements();
        l_filtre.setText("Filtres : ");
        filtre.add(l_filtre);
        String req = "SELECT * FROM docteur";
        
        try {
            gestionnaire.initializeStringV2();
        } catch (SQLException ex) {
        Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        gestionnaire.setHumanRequest(req);
        
        try {
            ArrayList<String> colonne = gestionnaire.getNomsColonne(req);
            
            for(int i=0; i<colonne.size(); i++) {
                choix_sous_recherche.addItem(colonne.get(i));
                filtres.add(new JCheckBox(colonne.get(i)));
                filtres.elementAt(i).setSelected(true);
                filtres.elementAt(i).setFont(new Font("Arial", Font.PLAIN, 18));
                filtre.add(filtres.elementAt(i));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        sous_recherche.add(choix_sous_recherche);
        sous_recherche.add(valider);
    }
    
    public void r_infirmier() {
        choix_sous_recherche.removeAllItems();
        l_sr.setText("Trier les infirmiers par...");
        filtre.removeAll();
        filtres.removeAllElements();
        l_filtre.setText("Filtres : ");
        filtre.add(l_filtre);
        String req = "SELECT * FROM infirmier";
        
        try {
            gestionnaire.initializeStringV2();
        } catch (SQLException ex) {
        Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        gestionnaire.setHumanRequest(req);
        
        try {
            ArrayList<String> colonne = gestionnaire.getNomsColonne(req);
            
            for(int i=0; i<colonne.size(); i++) {
                choix_sous_recherche.addItem(colonne.get(i));
                filtres.add(new JCheckBox(colonne.get(i)));
                filtres.elementAt(i).setSelected(true);
                filtres.elementAt(i).setFont(new Font("Arial", Font.PLAIN, 18));
                filtre.add(filtres.elementAt(i));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        sous_recherche.add(choix_sous_recherche);
        sous_recherche.add(valider);
    }
    
    public void r_malade() {
        choix_sous_recherche.removeAllItems();
        l_sr.setText("Trier les patients par...");
        filtre.removeAll();
        filtres.removeAllElements();
        l_filtre.setText("Filtres : ");
        filtre.add(l_filtre);
        String req = "SELECT * FROM malade";
        
        try {
            gestionnaire.initializeStringV2();
        } catch (SQLException ex) {
        Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        gestionnaire.setHumanRequest(req);
        
        try {
            ArrayList<String> colonne = gestionnaire.getNomsColonne(req);
            
            for(int i=0; i<colonne.size(); i++) {
                choix_sous_recherche.addItem(colonne.get(i));
                filtres.add(new JCheckBox(colonne.get(i)));
                filtres.elementAt(i).setSelected(true);
                filtres.elementAt(i).setFont(new Font("Arial", Font.PLAIN, 18));
                filtre.add(filtres.elementAt(i));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        sous_recherche.add(choix_sous_recherche);
        sous_recherche.add(valider);
    }
    
    public void r_employe() {
        choix_sous_recherche.removeAllItems();
        l_sr.setText("Trier les employés par...");
        filtre.removeAll();
        filtres.removeAllElements();
        l_filtre.setText("Filtres : ");
        filtre.add(l_filtre);
        String req = "SELECT * FROM employe";
        
        try {
            gestionnaire.initializeStringV2();
        } catch (SQLException ex) {
        Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        gestionnaire.setHumanRequest(req);
        
        try {
            ArrayList<String> colonne = gestionnaire.getNomsColonne(req);
            
            for(int i=0; i<colonne.size(); i++) {
                choix_sous_recherche.addItem(colonne.get(i));
                filtres.add(new JCheckBox(colonne.get(i)));
                filtres.elementAt(i).setSelected(true);
                filtres.elementAt(i).setFont(new Font("Arial", Font.PLAIN, 18));
                filtre.add(filtres.elementAt(i));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        sous_recherche.add(choix_sous_recherche);
        sous_recherche.add(valider);
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        
        if(e.getSource()==choix_recherche) {
            JComboBox<String> combo = (JComboBox<String>) e.getSource();
            String selected = (String) combo.getSelectedItem();
            selected_recherche = selected;

            if (selected.equals("service")) {
                r_par_service();
            } else if (selected.equals("chambre")) {
                r_chambre();
            } else if (selected.equals("docteur")) {
                r_docteur();
            } else if (selected.equals("infirmier")) {
                r_infirmier();
            } else if (selected.equals("malade")) {
                r_malade();
            } else if (selected.equals("employé")) {
                r_employe();
            }
        }
        else if(e.getSource()==choix_sous_recherche) {
            JComboBox<String> combo = (JComboBox<String>) e.getSource();
            String selected = (String) combo.getSelectedItem();
            if(selected_recherche.equals("service")) {
                requete = " FROM service WHERE nom = '" + selected + "'";
                System.out.println("requete dans service : " + requete);
            } else if(selected_recherche.equals("chambre")) {
                requete = " FROM chambre ORDER BY " + selected;
                System.out.println("requete chambre : " + requete);
            } else if(selected_recherche.equals("docteur")) {
                requete = " FROM docteur ORDER BY " + selected;
                System.out.println("requete docteur : " + requete);
            } else if(selected_recherche.equals("infirmier")) {
                requete = " FROM infirmier ORDER BY " + selected;
                System.out.println("requete inf : " + requete);
            } else if(selected_recherche.equals("malade")) {
                requete = " FROM malade ORDER BY " + selected;
                System.out.println("requete malade : " + requete);
            } else if(selected_recherche.equals("employé")) {
                requete = " FROM employe ORDER BY " + selected;
                System.out.println("requete employe : " + requete);
            }
        }
        else if(e.getSource()==valider) {
            //String sql_request = entreeUtilisateur.getText(), check = sql_request;
            String check = requete;
            String cond = "";
            String req = "";
            int compt = 0;
            for(int i=0; i<filtres.size(); i++) {
                if(filtres.elementAt(i).isSelected()) {
                    if(compt != 0) {
                        cond += ",";
                    }
                    cond += filtres.elementAt(i).getText();
                    compt++;
                }
            }
            if(compt == 0) {
                req = "SELECT *" + requete;
            } else {
                req = "SELECT " + cond + requete;
            }
            //System.out.println(requete);
            System.out.println(req);
            try {
                    gestionnaire.initializeStringV2();
                } catch (SQLException ex) {
                Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
                }
            //gestionnaire.setHumanRequest(sql_request);
            //gestionnaire.setHumanRequest(requete);
            gestionnaire.setHumanRequest(req);
    //        try {
    //            gestionnaire.parseHumanRequest();
    //        } catch (IOException ex) {
    //            Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
    //        } catch (SQLException ex) {
    //            Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
    //        }
            //entreeUtilisateur.setEditable(false);
            //valider.setEnabled(false);
            try {
                //ArrayList<String> resultat = gestionnaire.resultatRequete(sql_request);
                //ArrayList<String> resultat = gestionnaire.resultatRequete(requete);
                ArrayList<String> resultat = gestionnaire.resultatRequete(req);
                //ArrayList<String> colonne = gestionnaire.getNomColonne(sql_request);
                //ArrayList<String> colonne = gestionnaire.getNomsColonne(requete);
                ArrayList<String> colonne = gestionnaire.getNomsColonne(req);
                //String[] table_columns = new String[colonne.size()+1];
                String[] table_columns = new String[colonne.size()];
                //String[][] data_table = new String[resultat.size()/colonne.size()][colonne.size()+1];
                String[][] data_table = new String[resultat.size()/colonne.size()][colonne.size()];
                //table_columns[0] = "#";
                /*for(int i = 1; i < colonne.size()+1; ++i)
                {
                    table_columns[i] = colonne.get(i-1);
                }
                for(int i = 0; i < resultat.size()/colonne.size(); ++i)
                {
                    data_table[i][0] = Integer.toString(i + 1);
                    for(int j = 1; j < colonne.size()+1; ++j)
                    {
                        data_table[i][j] = resultat.get((i*colonne.size())+(j-1));
                    }
                }*/
                
                for(int i = 0; i < colonne.size(); ++i) {
                    table_columns[i] = colonne.get(i);
                }
                for(int i = 0; i < resultat.size()/colonne.size(); ++i) {
                    for(int j = 0; j < colonne.size(); ++j) {
                        data_table[i][j] = resultat.get((i*colonne.size())+(j));
                    }
                }

                final JTable onscreen = new JTable(data_table, table_columns);
                /*panel.removeAll();
                
                panel.add(onscreen.getTableHeader(), BorderLayout.PAGE_START);
                panel.add(onscreen, BorderLayout.CENTER);
                JScrollPane scrollPane = new JScrollPane(onscreen);
                panel.add(scrollPane);*/
                error = new JLabel();
                panel.add(error);
                ResRecherche resultat_recherche = new ResRecherche(onscreen, resultat.size()/colonne.size());
                resultat_recherche.setVisible(true);
            } catch (SQLException ex) {
                //entreeUtilisateur.setText("Problème avec la requete SQL! Veuillez réessayer!");
                //error.setText("Problème avec la requete SQL! Veuillez réessayer!");
                Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
            }
                    ///Pour toi Morgane ;)
            /*if(check.equals("qui a la plus grosse bite"))entreeUtilisateur.setText("le bg qui code tout ça, soyez pas jaloux");
            revalidate();*/
            //Je valide ;)
            /*final JTable onscreen = new JTable(data_table, table_columns);
            pan.removeAll();
            onscreen.setDefaultEditor(Object.class, null);
            pan.add(onscreen.getTableHeader(), BorderLayout.PAGE_START);
            pan.add(onscreen, BorderLayout.CENTER);
            JScrollPane scrollPane = new JScrollPane(onscreen);
            pan.add(scrollPane);*/
        /*} catch (SQLException ex) {
            entreeUtilisateur.setText("Problème avec la requete SQL! Veuillez réessayer!");
            Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }*/
    }
}
}
