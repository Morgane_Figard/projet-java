/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.border.EtchedBorder;
import modele.GestionBDD;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset; 
import org.jfree.data.category.DefaultCategoryDataset; 
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

/**
 *
 * @author Morgane
 */
public class Reporting extends JFrame {
    
    JPanel panel;
    ChartPanel mutuelles;   //pie
    ChartPanel rotation;    //bar
    ChartPanel sal_moy;     //bar      
    ChartPanel lits;        //pie
    ChartPanel inf_malade; //bar
    ChartPanel doct;        //bar
    private final GestionBDD gestionnaire;
    
    public Reporting() throws SQLException, ClassNotFoundException {
        setTitle("Reporting");
        setSize(1500,900);
        try{
            setIconImage(ImageIO.read(new File("pie-chart-icon.png")));
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        gestionnaire = new GestionBDD();
        
        panel = new JPanel();
        
        setMutuelles();
        setRotation();
        setLits();
        setDoct();
        setInfMalade();
        setSalMoy();
        
        mutuelles.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        rotation.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        lits.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        doct.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        inf_malade.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        sal_moy.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        
        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        
        c.gridwidth = 1;    //largeur = une seule colonne
        c.weightx = .01;
        c.weighty = .2;
        c.gridx = 0;    //première colonne
        c.gridy = 0;    //première ligne
        c.ipady = 0;
        c.ipadx = 0;
        c.insets = new Insets(20, 20, 20, 20);
        c.fill = GridBagConstraints.BOTH;
        
        panel.add(mutuelles, c);
              
        c.gridwidth = 1;    //largeur = une seule colonne
        c.weightx = .01;
        c.weighty = .2;
        c.gridx = 1;    //deuxième colonne
        c.gridy = 0;    //première ligne
        c.ipady = 0;
        c.ipadx = 0;
        c.insets = new Insets(20, 20, 20, 20);
        c.fill = GridBagConstraints.BOTH;
        
        //panel.add(new JLabel("2"), c);
        panel.add(rotation, c);
        
        c.gridwidth = 1;    //largeur = une seule colonne
        c.weightx = .01;
        c.weighty = .2;
        c.gridx = 2;    //troisième colonne
        c.gridy = 0;    //première ligne
        c.ipady = 0;
        c.ipadx = 0;
        c.insets = new Insets(20, 20, 20, 20);
        c.fill = GridBagConstraints.BOTH;
        
        panel.add(lits, c);
        
        c.gridwidth = 1;    //largeur = une seule colonne
        c.weightx = .01;
        c.weighty = .2;
        c.gridx = 0;    //première colonne
        c.gridy = 1;    //deuxième ligne
        c.ipady = 0;
        c.ipadx = 0;
        c.insets = new Insets(20, 20, 20, 20);
        c.fill = GridBagConstraints.BOTH;
        
        panel.add(doct, c);
        
        c.gridwidth = 1;    //largeur = une seule colonne
        c.weightx = .01;
        c.weighty = .2;
        c.gridx = 1;    //deuxième colonne
        c.gridy = 1;    //deuxième ligne
        c.ipady = 0;
        c.ipadx = 0;
        c.insets = new Insets(20, 20, 20, 20);
        c.fill = GridBagConstraints.BOTH;
        
        panel.add(inf_malade, c);
        
        c.gridwidth = 1;    //largeur = une seule colonne
        c.weightx = .01;
        c.weighty = .2;
        c.gridx = 2;    //troisième colonne
        c.gridy = 1;    //deuxième ligne
        c.ipady = 0;
        c.ipadx = 0;
        c.insets = new Insets(20, 20, 20, 20);
        c.fill = GridBagConstraints.BOTH;
        
        panel.add(sal_moy, c);
        
        getContentPane().add(panel);
    }
    
    public void setMutuelles() {
        String requete = "SELECT DISTINCT mutuelle FROM malade ORDER BY mutuelle";
        ArrayList<String> mutuelles_trouvees;
        int[] nb_pers;
        
        try {
            gestionnaire.initializeStringV2();
        } catch (SQLException ex) {
        Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //trouver toutes les mutuelles
        gestionnaire.setHumanRequest(requete);
        
        try {
            ArrayList<String> resultat = gestionnaire.resultatRequete(requete);
                        
            mutuelles_trouvees = resultat;
            
            nb_pers = new int[mutuelles_trouvees.size()];
                                 
            for(int i=0; i<mutuelles_trouvees.size(); i++) {
                //trouver le nombre de personnes par mutuelles
                requete = "SELECT * FROM malade WHERE mutuelle ='" + mutuelles_trouvees.get(i) + "'";
                gestionnaire.setHumanRequest(requete);

                try {
                    ArrayList<String> resultat2 = gestionnaire.resultatRequete(requete);
                    
                    nb_pers[i] = resultat2.size();

                } catch (SQLException ex) {
                    Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            DefaultPieDataset dataset = new DefaultPieDataset();
            for(int i=0; i<mutuelles_trouvees.size(); i++) {
                dataset.setValue(mutuelles_trouvees.get(i), nb_pers[i]);
                JFreeChart chart = ChartFactory.createPieChart(      
                    "Nombre de malades par mutuelles",   // chart title 
                    dataset,          // data    
                    false,             // don't include legend   
                    true, 
                    false);

                mutuelles = new ChartPanel(chart);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void setRotation() {
        final String jour = "Jour";        
        final String nuit = "Nuit";
        
        String requete = "SELECT DISTINCT code_service FROM chambre ORDER BY code_service";
        ArrayList<String> services;
        int[] nb_inf_jour;
        int[] nb_inf_nuit;
        
        try {
            gestionnaire.initializeStringV2();
        } catch (SQLException ex) {
        Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //trouver tous les services
        gestionnaire.setHumanRequest(requete);
        
        try {
            ArrayList<String> resultat = gestionnaire.resultatRequete(requete);
                        
            services = resultat;
            
            nb_inf_jour = new int[services.size()];
            nb_inf_nuit = new int[services.size()];
                                 
            for(int i=0; i<services.size(); i++) {
                //trouver le nb d'inf de jour par service
                requete = "SELECT * FROM infirmier WHERE code_service ='" + services.get(i) + "' AND rotation = 'JOUR'";
                gestionnaire.setHumanRequest(requete);

                try {
                    ArrayList<String> resultat2 = gestionnaire.resultatRequete(requete);
                    
                    nb_inf_jour[i] = resultat2.size();

                } catch (SQLException ex) {
                    Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                //trouver le nb d'inf de nuit par service
                requete = "SELECT * FROM infirmier WHERE code_service ='" + services.get(i) + "' AND rotation = 'NUIT'";
                gestionnaire.setHumanRequest(requete);

                try {
                    ArrayList<String> resultat2 = gestionnaire.resultatRequete(requete);
                    
                    nb_inf_nuit[i] = resultat2.size();

                } catch (SQLException ex) {
                    Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            DefaultCategoryDataset dataset = new DefaultCategoryDataset();  
            for(int i=0; i<services.size(); i++) {
                dataset.addValue(nb_inf_jour[i], jour, services.get(i)); 
                dataset.addValue(nb_inf_nuit[i], nuit, services.get(i)); 
            }
            JFreeChart barChart = ChartFactory.createBarChart(
                "Rotation des infirmier(e)s selon le service",           
                "Service",            
                "Nombre",            
                dataset,          
                PlotOrientation.VERTICAL,           
                true, true, false);

            rotation = new ChartPanel(barChart);
            
        } catch (SQLException ex) {
            Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void setSalMoy() {
        String requete = "SELECT DISTINCT code_service FROM infirmier ORDER BY code_service";
        ArrayList<String> services;
        float[] sal_moy;
        String nb = "nb";
        
        try {
            gestionnaire.initializeStringV2();
        } catch (SQLException ex) {
        Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //trouver tous les services
        gestionnaire.setHumanRequest(requete);
        
        try {
            ArrayList<String> resultat = gestionnaire.resultatRequete(requete);
                        
            services = resultat;
            
            sal_moy = new float[services.size()];
                                 
            for(int i=0; i<services.size(); i++) {
                //trouver le nombre moyen de lits par service
                requete = "SELECT AVG(salaire) FROM infirmier WHERE code_service ='" + services.get(i) + "'";
                gestionnaire.setHumanRequest(requete);

                try {
                    ArrayList<String> resultat2 = gestionnaire.resultatRequete(requete);
                    
                    //besoin de couvertir le string en float
                    sal_moy[i] = Float.parseFloat(resultat2.get(0));
                } catch (SQLException ex) {
                    Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            DefaultCategoryDataset dataset = new DefaultCategoryDataset();  
            for(int i=0; i<services.size(); i++) {
                dataset.addValue(sal_moy[i], nb, services.get(i));  
            }
            
            JFreeChart barChart = ChartFactory.createBarChart(
                "Salaire moyen des infirmier(e)s par service",           
                null,            
                null,            
                dataset,          
                PlotOrientation.VERTICAL,           
                false, true, false);

            this.sal_moy = new ChartPanel(barChart);
            
        } catch (SQLException ex) {
            Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void setLits() {
        String requete = "SELECT DISTINCT code_service FROM chambre ORDER BY code_service";
        ArrayList<String> services;
        float[] nb_lits_moy;
        
        try {
            gestionnaire.initializeStringV2();
        } catch (SQLException ex) {
        Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //trouver tous les services
        gestionnaire.setHumanRequest(requete);
        
        try {
            ArrayList<String> resultat = gestionnaire.resultatRequete(requete);
                        
            services = resultat;
            
            nb_lits_moy = new float[services.size()];
                                 
            for(int i=0; i<services.size(); i++) {
                //trouver le nombre moyen de lits par service
                requete = "SELECT AVG(nb_lits) FROM chambre WHERE code_service ='" + services.get(i) + "'";
                gestionnaire.setHumanRequest(requete);

                try {
                    ArrayList<String> resultat2 = gestionnaire.resultatRequete(requete);
                    
                    //besoin de couvertir le string en float
                    nb_lits_moy[i] = Float.parseFloat(resultat2.get(0));
                } catch (SQLException ex) {
                    Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            DefaultPieDataset dataset = new DefaultPieDataset();
            for(int i=0; i<services.size(); i++) {
                dataset.setValue(services.get(i), nb_lits_moy[i]);
                JFreeChart chart = ChartFactory.createPieChart(      
                    "Nombre moyen de lits par chambre par service",   // chart title 
                    dataset,          // data    
                    false,             // don't include legend   
                    true, 
                    false);

                lits = new ChartPanel(chart);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void setInfMalade() {
        String requete = "SELECT DISTINCT code_service FROM infirmier ORDER BY code_service";
        ArrayList<String> services;
        int[] nb_inf;
        int[] nb_malades;
        String nb = "nb";
        
        try {
            gestionnaire.initializeStringV2();
        } catch (SQLException ex) {
        Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //trouver tous les services
        gestionnaire.setHumanRequest(requete);
        
        try {
            services = gestionnaire.resultatRequete(requete);
                        
            nb_inf = new int[services.size()];
            nb_malades = new int[services.size()];
                                 
            for(int i=0; i<services.size(); i++) {
                //trouver le nombre d'infirmiers par service
                requete = "SELECT * FROM infirmier WHERE code_service ='" + services.get(i) + "'";
                gestionnaire.setHumanRequest(requete);

                try {
                    ArrayList<String> resultat2 = gestionnaire.resultatRequete(requete);
                    
                    nb_inf[i] = resultat2.size();

                } catch (SQLException ex) {
                    Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                //trouver le nombre de malades par service
                requete = "SELECT * FROM hospitalisation WHERE code_service ='" + services.get(i) + "'";
                gestionnaire.setHumanRequest(requete);

                try {
                    ArrayList<String> resultat2 = gestionnaire.resultatRequete(requete);
                    
                    nb_malades[i] = resultat2.size();

                } catch (SQLException ex) {
                    Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            DefaultCategoryDataset dataset = new DefaultCategoryDataset();  
            for(int i=0; i<services.size(); i++) {
                float rapport = (float) nb_inf[i]/nb_malades[i];
                dataset.addValue(rapport, nb, services.get(i));  
            }
            
            JFreeChart barChart = ChartFactory.createBarChart(
                "Nombre d'infirmier(e)s par malades par service",           
                null,            
                null,            
                dataset,          
                PlotOrientation.VERTICAL,           
                false, true, false);

            inf_malade = new ChartPanel(barChart);
            
        } catch (SQLException ex) {
            Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void setDoct() {
        final String nb = "nb";
        final String doct_no = "Aucun malade hospitalisé";
        final String doct_oui = "Au moins un malade hospitalisé";
        int docteurs_no_malade;
        int docteurs_malades;
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();  

        try {
            gestionnaire.initializeStringV2();
        } catch (SQLException ex) {
        Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //trouver le nb de docteurs qui n'ont aucun malade hospitalisé
        String requete = "SELECT employe.prenom, employe.nom FROM employe, docteur WHERE employe.numero NOT IN (SELECT soigne.no_docteur FROM soigne, hospitalisation WHERE soigne.no_malade = hospitalisation.no_malade) AND employe.numero = docteur.numero";
        gestionnaire.setHumanRequest(requete);
        
        try {
            ArrayList<String> resultat = gestionnaire.resultatRequete(requete);
            docteurs_no_malade = resultat.size();
            dataset.addValue(docteurs_no_malade, nb, doct_no);
        } catch (SQLException ex) {
            Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //trouver le nb de docteurs qui ont au moins un malade hospitalisé
        requete = "SELECT employe.prenom, employe.nom FROM employe WHERE employe.numero IN (SELECT soigne.no_docteur FROM soigne, hospitalisation WHERE soigne.no_malade = hospitalisation.no_malade )";
        gestionnaire.setHumanRequest(requete);
        
        try {
            ArrayList<String> resultat = gestionnaire.resultatRequete(requete);
            docteurs_malades = resultat.size();
            dataset.addValue(docteurs_malades, nb, doct_oui);
        } catch (SQLException ex) {
            Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        JFreeChart barChart = ChartFactory.createBarChart(
            "Nombre de docteurs ayant ou non des malades hospitalisés",           
            null,            
            null,            
            dataset,          
            PlotOrientation.VERTICAL,           
            false, true, false);

        doct = new ChartPanel(barChart);
    }
}
