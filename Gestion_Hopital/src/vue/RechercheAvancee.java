/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.table.DefaultTableModel;
import modele.GestionBDD;
import modele.*;

/**
 *
 * @author Morgane
 */
public class RechercheAvancee extends JFrame {

    JPanel panel;
    JPanel legende;
    JPanel recherche;
    JLabel instructions;
    JTextArea entreeUtilisateur;
    JButton valider;
    GestionBDD gestionnaire;

    public RechercheAvancee() {
        setSize(650, 350);
        try {
            setIconImage(ImageIO.read(new File("search-icon.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setTitle("Recherche avancée");
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        recherche = new JPanel();
        recherche.setLayout(new FlowLayout());
        legende = new JPanel();
        legende.setLayout(new FlowLayout());

        instructions = new JLabel();
        instructions.setText("Entrez votre recherche : ");
        instructions.setFont(new Font("Arial", Font.PLAIN, 18));

        legende.add(instructions);

        entreeUtilisateur = new JTextArea();
        entreeUtilisateur.setLineWrap(true);
        entreeUtilisateur.setWrapStyleWord(true);
        entreeUtilisateur.setEnabled(false);
        entreeUtilisateur.setBorder(BorderFactory.createLineBorder(Color.black));
        entreeUtilisateur.setFont(new Font("Arial", Font.PLAIN, 18));
        /*entreeUtilisateur.setText("Entrez une phrase ou des mots clés.\n"
                        + "Vous pouvez aussi ecrire votre requete directement en SQL");*/
        entreeUtilisateur.setText("Cliquez ici pour entrer votre recherche");
        entreeUtilisateur.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2 && !e.isConsumed()) {
                    e.consume();
                    entreeUtilisateur.setEnabled(true);
                    entreeUtilisateur.setEditable(true);
                    entreeUtilisateur.setText("");
                }
            }
        });
        entreeUtilisateur.setEditable(true);

        recherche.add(entreeUtilisateur);

        valider = new JButton("Valider");
        valider.setFont(new Font("Arial", Font.PLAIN, 18));
        valider.addActionListener((ActionEvent e) -> {
            try {
                gestionnaire = new GestionBDD();
                gestionnaire.setHumanRequest(entreeUtilisateur.getText());
                try {
                    gestionnaire.parseHumanRequest();
                } catch (IOException ex) {
                    Logger.getLogger(RechercheAvancee.class.getName()).log(Level.SEVERE, null, ex);
                }
                panel.removeAll();
                ArrayList<String> resultat = gestionnaire.resultatRequete(gestionnaire.getString());
                ArrayList<String> colonne = gestionnaire.getNomsColonne(gestionnaire.getString());
                String[] table_columns = new String[colonne.size()+1];
                String[][] data_table = new String[resultat.size()/colonne.size()][colonne.size()+1];
                table_columns[0] = "#";
                for(int i = 1; i < colonne.size()+1; ++i)
                {
                    table_columns[i] = colonne.get(i-1);
                }

                for(int i = 0; i < resultat.size()/colonne.size(); ++i)
                {
                    data_table[i][0] = Integer.toString(i + 1);
                    for(int j = 1; j < colonne.size()+1; ++j)
                    {
                        data_table[i][j] = resultat.get((i*colonne.size())+(j-1));
                    }
                }
                final JTable onscreen = new JTable(data_table, table_columns);
                ResRecherche res = new ResRecherche(onscreen, -1);
                valider.setEnabled(true);
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, "Je n'ai pas bien compris la requete, pouvez-vous réessayer?");
                valider.setEnabled(true);
            } catch (ClassNotFoundException ex) {
                
            }
        });

        recherche.add(valider);

        panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        c.gridwidth = 1;    //largeur = une seule colonne
        c.weightx = .01;
        c.weighty = .2;
        c.gridx = 0;    //première colonne
        c.gridy = 0;    //première ligne
        c.ipady = 20;
        c.ipadx = 10;
        c.insets = new Insets(40, 10, 10, 0);

        panel.add(legende, c);
        
        c.gridwidth = 1;    //largeur = une seule colonne
        c.weightx = .01;
        c.weighty = .2;
        c.gridx = 0;    //première colonne
        c.gridy = 1;    //deuxième ligne
        c.ipady = 20;
        c.ipadx = 10;
        c.insets = new Insets(10, 10, 50, 0);

        panel.add(recherche, c);
        panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                entreeUtilisateur.setEnabled(false);
                entreeUtilisateur.setText("Cliquez ici pour entrer votre recherche");
            }
        });
        getContentPane().add(panel);
    }
}
