/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import java.awt.*;
import javax.swing.*;

/**
 *
 * @author Morgane
 */
public class ResRecherche extends JFrame {
    JPanel panel;
    JPanel main_panel;
    JTable onscreen;
    JLabel nb_res;
    
    public ResRecherche(JTable table, int nb_resultats) {
        setTitle("Résultat de la recherche");
        setSize(1000, 600);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        panel = new JPanel();
        panel.setLayout(new BorderLayout());
        
        onscreen = new JTable();
        onscreen = table;
        
        onscreen.setDefaultEditor(Object.class, null);
        onscreen.setFont(new Font("Arial", Font.PLAIN, 16));
        onscreen.getTableHeader().setFont(new Font("Arial", Font.PLAIN, 16));
        panel.add(onscreen.getTableHeader(), BorderLayout.PAGE_START);
        panel.add(onscreen, BorderLayout.CENTER);
        JScrollPane scrollPane = new JScrollPane(onscreen);
        panel.add(scrollPane);
        
        //getContentPane().add(panel);
        main_panel = new JPanel();
        //main_panel.setLayout(new BorderLayout());
        main_panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        
        if(nb_resultats >= 0) {
            nb_res = new JLabel("<html><b>" + nb_resultats + "</b> resultat(s)</html>");
            nb_res.setFont(new Font("Arial", Font.PLAIN, 18));
            //main_panel.add(nb_res, BorderLayout.NORTH);
            c.gridwidth = 1;    //largeur = une seule colonne
            c.weightx = .01;
            c.weighty = .2;
            c.gridx = 0;    //première colonne
            c.gridy = 0;    //première ligne
            c.ipady = 0;
            c.ipadx = 10;
            c.insets = new Insets(5, 5, 5, 0);
            c.anchor = GridBagConstraints.LINE_START;
            c.fill = GridBagConstraints.BOTH;
            
            main_panel.add(nb_res, c);
        }
        
        //main_panel.add(panel, BorderLayout.CENTER);
        c.gridwidth = 1;    //largeur = une seule colonne
        c.weightx = .01;
        c.weighty = .2;
        c.gridx = 0;    //première colonne
        c.gridy = 1;    //2e ligne
        c.ipady = 0;
        c.ipadx = 0;
        c.insets = new Insets(0, 10, 10, 10);
        c.fill = GridBagConstraints.BOTH;
        main_panel.add(panel, c);
        
        getContentPane().add(main_panel);
        
        setVisible(true);
    }
}
