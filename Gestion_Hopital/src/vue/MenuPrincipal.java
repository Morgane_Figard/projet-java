/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;
import controleur.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author Morgane
 */
public class MenuPrincipal extends JFrame implements ActionListener {
    
    JPanel panel;
    JButton recherche;
    JButton maj;
    JButton reporting;
    Hopital projet;
    
    public MenuPrincipal() {
        
        panel = new JPanel();
        
        Icon recherche_icon = new ImageIcon("search-icon.png");
        recherche = new JButton(" Recherche d'informations", recherche_icon);
        
        Icon maj_icon = new ImageIcon("edit-icon.png");
        maj = new JButton(" Mise à jour des données", maj_icon);
        
        Icon reporting_icon = new ImageIcon("pie-chart-icon.png");
        reporting = new JButton(" Reporting", reporting_icon);
        
        projet = new Hopital();

        setTitle("Centre hospitalier Les Fleurs");
        setSize(800,400);
        try{
            setIconImage(ImageIO.read(new File("hospital-red.png")));
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        recherche.setFont(new Font("Arial", Font.PLAIN, 20));
        recherche.addActionListener(this);
        recherche.addMouseListener(new MouseAdapter() {
            public void mouseExited(MouseEvent e) {
                if(e.getSource()==recherche) {
                    recherche.setBackground(new JButton().getBackground());
                }
            }

            public void mouseEntered(MouseEvent e) {
                if(e.getSource()==recherche) {
                    recherche.setBackground(new Color(150, 143, 201));
                }
            }
        });
        
        maj.setFont(new Font("Arial", Font.PLAIN, 20));
        maj.addActionListener(this);
        maj.addMouseListener(new MouseAdapter() {
            public void mouseExited(MouseEvent e) {
                if(e.getSource()==maj) {
                    maj.setBackground(new JButton().getBackground());
                }
            }

            public void mouseEntered(MouseEvent e) {
                if(e.getSource()==maj) {
                    maj.setBackground(new Color(150, 143, 201));
                }
            }
        });
        
        reporting.setFont(new Font("Arial", Font.PLAIN, 20));
        reporting.addActionListener(this);
        reporting.addMouseListener(new MouseAdapter() {
            public void mouseExited(MouseEvent e) {
                if(e.getSource()==reporting) {
                    reporting.setBackground(new JButton().getBackground());
                }
            }

            public void mouseEntered(MouseEvent e) {
                if(e.getSource()==reporting) {
                    reporting.setBackground(new Color(150, 143, 201));
                }
            }
        });
        
        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
                
        c.gridwidth = 1;    //une seule colonne
        c.weightx = .01;
        c.weighty = .2;
        c.gridx = 0;    //première colonne
        c.gridy = 0;    //première ligne
        c.ipady = 20;
        c.ipadx = 10;
        
        panel.add(recherche, c);
        
        c.gridwidth = 1;    //une seule colonne
        c.weightx = .01;
        c.weighty = .2;
        c.gridx = 1;    //première colonne
        c.gridy = 0;    //deuxième ligne
        c.ipady = 20;
        c.ipadx = 10;
        panel.add(maj, c);
        
        c.gridwidth = 2;    //une seule colonne
        c.weightx = .01;
        c.weighty = .2;
        c.gridx = 0;    //première colonne
        c.gridy = 1;    //troisième ligne
        c.ipady = 20;
        c.ipadx = 10;
        panel.add(reporting, c);
        
        getContentPane().add(panel);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == recherche) {
            try {
                projet.recherche();
            } catch (SQLException ex) {
                Logger.getLogger(MenuPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(MenuPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (e.getSource() == maj) {
            try {
                projet.maj();
            } catch (SQLException ex) {
                Logger.getLogger(MenuPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (e.getSource() == reporting) {
            try {
                projet.reporting();
            } catch (SQLException ex) {
                Logger.getLogger(MenuPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(MenuPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
