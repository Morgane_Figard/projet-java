/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import modele.GestionBDD;

/**
 *
 * @author Morgane
 */
public class TableMaj extends JFrame implements ActionListener {

    JPanel three_buttons;
    JPanel pan;
    JTable onscreen;
    JButton ajouter;
    JButton supprimer;
    String nom_table;
    ArrayList<String> colonne;
    GestionBDD gestionnaire;
    String[] table_columns;
    String currently_edited;

    public TableMaj(JTable table, String nom, ArrayList<String> colonne, GestionBDD gestionnaire) throws SQLException, ClassNotFoundException {
        setTitle("Table à mettre à jour");
        setSize(800, 600);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        DefaultTableModel model = (DefaultTableModel) table.getModel();
        onscreen = new JTable(model) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return column != 0;
            }
        };
        onscreen.setFont(new Font("Arial", Font.PLAIN, 16));
        onscreen.getTableHeader().setFont(new Font("Arial", Font.PLAIN, 16));

        pan = new JPanel();
        pan.setLayout(new BorderLayout());

        three_buttons = new JPanel();
        ajouter = new JButton("Ajouter un champ");
        ajouter.setFont(new Font("Arial", Font.PLAIN, 18));

        nom_table = new String("");
        nom_table = nom;

        this.colonne = new ArrayList<String>();
        this.colonne = colonne;

        this.gestionnaire = new GestionBDD();
        this.gestionnaire = gestionnaire;
        table_columns = new String[colonne.size() + 1];

        add(three_buttons, "South");
        add(pan, "North");

        ajouter.addActionListener((ActionEvent e) -> {
            try {
                DefaultTableModel modelajout = (DefaultTableModel) onscreen.getModel();
                String[] new_Row = new String[colonne.size()];
                for (int i = 0; i < colonne.size(); ++i) {
                    new_Row[i] = " ";
                }
                new_Row[0] = Integer.toString(modelajout.getRowCount() + 1);
                modelajout.addRow(new_Row);
                String insertIntoStatement = "INSERT INTO " + nom_table;
                String valuesStatement = " VALUES(";
                for (int i = 1; i < colonne.size() + 1; ++i) {
                    valuesStatement += "0";
                    if (i != colonne.size()) {
                        valuesStatement += ",";
                    } else {
                        valuesStatement += ")";
                    }
                }
                gestionnaire.miseajour(insertIntoStatement + valuesStatement);
            } catch (SQLException ex) {
                Logger.getLogger(TableMaj.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        supprimer = new JButton("Supprimer la ligne");
        supprimer.setFont(new Font("Arial", Font.PLAIN, 18));
        supprimer.addActionListener((ActionEvent e) -> {
            try {
                table_columns[0] = "#";
                for (int i = 1; i < colonne.size() + 1; ++i) {
                    table_columns[i] = colonne.get(i - 1);
                }
                int row = onscreen.getSelectedRow();
                String[] condition = new String[colonne.size()];
                for (int i = 1; i < colonne.size() + 1; ++i) {
                    condition[i - 1] = (String) onscreen.getValueAt(row, i);
                }
                DefaultTableModel modelsuppr = (DefaultTableModel) onscreen.getModel();
                modelsuppr.removeRow(row);
                for (int i = 0; i < onscreen.getRowCount(); ++i) {
                    onscreen.setValueAt(Integer.toString(i + 1), i, 0);
                }
                String deleteStatement = "DELETE FROM " + nom_table;
                String whereStatement = " WHERE ";
                for (int i = 1; i < colonne.size() + 1; ++i) {
                    whereStatement += table_columns[i] + "='" + condition[i - 1] + "'";
                    if (i != colonne.size()) {
                        whereStatement += " AND ";
                    }
                }
                gestionnaire.miseajour(deleteStatement + whereStatement);
            } catch (SQLException ex) {
                Logger.getLogger(Maj.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        model.addTableModelListener((TableModelEvent e) -> {
            try {
                if (TableMaj.isNumeric(currently_edited)
                        == TableMaj.isNumeric((String) onscreen.getValueAt(onscreen.getEditingRow(), onscreen.getEditingColumn()))) {
                    String columnChanged = onscreen.getColumnName(onscreen.getEditingColumn());
                    String[] allValues = new String[onscreen.getColumnCount() - 1];
                    for (int i = 1; i < onscreen.getColumnCount(); ++i) {
                        allValues[i - 1] = (String) onscreen.getValueAt(onscreen.getEditingRow(), i);
                    }
                    String updateStatement = "UPDATE " + nom_table;
                    String setStatement = " SET " + columnChanged + "='"
                            + allValues[onscreen.getEditingColumn() - 1] + "'";
                    String whereStatement = " WHERE ";
                    for (int i = 1; i < onscreen.getColumnCount(); ++i) {
                        if (i != onscreen.getEditingColumn()) {
                            whereStatement += onscreen.getColumnName(i) + "='" + allValues[i - 1] + "'";
                        } else {
                            whereStatement += onscreen.getColumnName(i) + "='" + currently_edited + "'";
                        }
                        if (i != colonne.size()) {
                            whereStatement += " AND ";
                        }
                    }
                    //JOptionPane.showMessageDialog(this, updateStatement + setStatement + whereStatement);
                    gestionnaire.miseajour(updateStatement + setStatement + whereStatement);
                    if (nom_table.equals("employe") && columnChanged.equals("numero")) {
                        gestionnaire.miseajour("UPDATE infirmier SET numero='"
                                + allValues[onscreen.getEditingColumn() - 1] + "' WHERE numero='" + currently_edited + "'");
                        gestionnaire.miseajour("UPDATE docteur SET numero='"
                                + allValues[onscreen.getEditingColumn() - 1] + "' WHERE numero='" + currently_edited + "'");
                    }
                    if (nom_table.equals("docteur") && columnChanged.equals("numero")) {
                        gestionnaire.miseajour("UPDATE infirmier SET numero='"
                                + allValues[onscreen.getEditingColumn() - 1] + "' WHERE numero='" + currently_edited + "'");
                        gestionnaire.miseajour("UPDATE employe SET numero='"
                                + allValues[onscreen.getEditingColumn() - 1] + "' WHERE numero='" + currently_edited + "'");
                    }
                    if (nom_table.equals("infirmier") && columnChanged.equals("numero")) {
                        gestionnaire.miseajour("UPDATE employe SET numero='"
                                + allValues[onscreen.getEditingColumn() - 1] + "' WHERE numero='" + currently_edited + "'");
                        gestionnaire.miseajour("UPDATE docteur SET numero='"
                                + allValues[onscreen.getEditingColumn() - 1] + "' WHERE numero='" + currently_edited + "'");
                    }
                    if (nom_table.equals("malade") && columnChanged.equals("numero")) {
                        gestionnaire.miseajour("UPDATE hospitalisation SET no_malade='"
                                + allValues[onscreen.getEditingColumn() - 1] + "' WHERE no_malade ='" + currently_edited + "'");
                    }
                    if (nom_table.equals("service") && columnChanged.equals("code")) {
                        gestionnaire.miseajour("UPDATE hospitalisation SET code_service='"
                                + allValues[onscreen.getEditingColumn() - 1] + "' WHERE code_service='" + currently_edited + "'");
                        gestionnaire.miseajour("UPDATE infirmier SET code_service='"
                                + allValues[onscreen.getEditingColumn() - 1] + "' WHERE code_service='" + currently_edited + "'");
                        gestionnaire.miseajour("UPDATE chambre SET code_service='"
                                + allValues[onscreen.getEditingColumn() - 1] + "' WHERE code_service='" + currently_edited + "'");
                    }
                    if (nom_table.equals("chambre") && columnChanged.equals("no_chambre")) {
                        gestionnaire.miseajour("UPDATE hospitalisation SET no_chambre='"
                                + allValues[onscreen.getEditingColumn() - 1] + "' WHERE no_chambre='" + currently_edited + "'");
                    }
                    if (nom_table.equals("chambre") && columnChanged.equals("nb_lit")) {
                        gestionnaire.miseajour("UPDATE hospitalisation SET lit='"
                                + allValues[onscreen.getEditingColumn() - 1] + "' WHERE lit='" + currently_edited + "'");
                    }
                } else {
                    JOptionPane.showMessageDialog(pan, "        Pas le bon format!");
                    onscreen.setValueAt(currently_edited, onscreen.getEditingRow(), onscreen.getEditingColumn());
                }
            } catch (SQLException ex) {
                Logger.getLogger(TableMaj.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        three_buttons.add(ajouter, "WEST");
        three_buttons.add(supprimer, "EAST");
        pan.add(onscreen.getTableHeader(), BorderLayout.PAGE_START);
        pan.add(onscreen, BorderLayout.CENTER);

        JScrollPane scrollPane = new JScrollPane(onscreen);
        pan.add(scrollPane);

        onscreen.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseExited(MouseEvent e) { ///oui c'est bien mouseExited jsp pq ça marche mais ça marche
                ///DONC PAS TOUCHE svp
                if (e.getClickCount() == 2 && !e.isConsumed()) {
                    e.consume();
                    currently_edited = (String) onscreen.getValueAt(onscreen.getEditingRow(),
                            onscreen.getEditingColumn());
                    //JOptionPane.showMessageDialog(pan,currently_edited);
                }
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ///Empty method (needed to implement ActionListener)
    }

    public static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
