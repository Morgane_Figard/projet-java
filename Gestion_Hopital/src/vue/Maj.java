package vue;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import modele.GestionBDD;

public class Maj extends JFrame implements ActionListener {

    /*private JTextField instructionsMaj;
    private JTextArea entreeUtilisateur;
    private JButton valider;
    private final GestionBDD gestionnaire;
    private JScrollPane scroll;
    private JPanel pan;*/
    JPanel panel;
    JPanel sel_table;
    JLabel l_sel_table;
    JComboBox<String> choix_table;
    JButton valider;
    private final GestionBDD gestionnaire;
    String selected_table;
    String requete;

    public Maj() throws SQLException, ClassNotFoundException {
        gestionnaire = new GestionBDD();
        setSize(650, 350);
        try{
            setIconImage(ImageIO.read(new File("edit-icon.png")));
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        setTitle("Mise à jour des données");
        setLocationRelativeTo(null);
        setResizable(false);
        //setDefaultCloseOperation(2);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        sel_table = new JPanel();
        sel_table.setLayout(new FlowLayout());
        
        l_sel_table = new JLabel();
        l_sel_table.setText("Sélectionnez une table à modifier : ");
        l_sel_table.setFont(new Font("Arial", Font.PLAIN, 18));
        
        sel_table.add(l_sel_table);
        ArrayList<String>a_tables = gestionnaire.resultatRequete("SHOW TABLES");
        a_tables.add(" ");
        Collections.sort(a_tables);
        String[]tables = a_tables.toArray(new String[a_tables.size()]);
        choix_table = new JComboBox(tables);
        choix_table.setFont(new Font("Arial", Font.PLAIN, 18));
        choix_table.addActionListener(this);
        selected_table = new String("");
        
        sel_table.add(choix_table);
        
        valider = new JButton("Valider");
        valider.addActionListener(this);
        valider.setFont(new Font("Arial", Font.PLAIN, 18));
                
        sel_table.add(valider);
        
        panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        
        c.gridwidth = 1;    //largeur = une seule colonne
        c.weightx = .01;
        c.weighty = .2;
        c.gridx = 0;    //première colonne
        c.gridy = 0;    //première ligne
        c.ipady = 20;
        c.ipadx = 10;
        c.insets = new Insets(40, 10, 50, 0);
        
        panel.add(sel_table, c);
        
        getContentPane().add(panel);
        
        requete = new String("");
    }

    /*public void setFields() {
        pan.setLayout(new BorderLayout());
        instructionsMaj = new JTextField();
        entreeUtilisateur = new JTextArea();
        valider = new JButton("Valider");
        scroll = new JScrollPane(entreeUtilisateur);

        instructionsMaj.setText("Quelle table voulez-vous modifer ?");
        entreeUtilisateur.setText("");

        pan.add(instructionsMaj, "North");
        pan.add(entreeUtilisateur);
        pan.add(valider, "South");

        valider.addActionListener(this);
        entreeUtilisateur.setAlignmentY(TOP_ALIGNMENT);
        scroll.setAlignmentX(entreeUtilisateur.getAlignmentX());
        instructionsMaj.setEditable(false);
        entreeUtilisateur.setEditable(true);

        entreeUtilisateur.setLineWrap(true);
        entreeUtilisateur.setWrapStyleWord(true);

        getContentPane().add(pan);
    }*/

    public void actionPerformed(ActionEvent e) {
        //String sql_request = "SELECT * FROM " + entreeUtilisateur.getText();
        //String table_name = entreeUtilisateur.getText();
        if(e.getSource()==choix_table) {
            JComboBox<String> combo = (JComboBox<String>) e.getSource();
            String selected = (String) combo.getSelectedItem();
            selected_table = selected;
        }/*
        gestionnaire.setHumanRequest(sql_request);
        entreeUtilisateur.setEditable(false);
        pan.remove(instructionsMaj);
        valider.setEnabled(false);
        try {
            pan.removeAll();
            JPanel three_buttons = new JPanel();
            add(three_buttons, "South");
            add(pan, "North");
            JButton ajouter = new JButton("Ajouter un champ");
            JButton supprimer = new JButton("Supprimer la ligne");

            ArrayList<String> resultat = gestionnaire.resultatRequete(sql_request);
            ArrayList<String> colonne = gestionnaire.getNomsColonne(sql_request);

            String[] table_columns = new String[colonne.size() + 1];
            String[][] data_table = new String[resultat.size() / colonne.size()][colonne.size() + 1];

            table_columns[0] = "#";
            for (int i = 1; i < colonne.size() + 1; ++i) {
                table_columns[i] = colonne.get(i - 1);}*/

        else if(e.getSource()==valider) {
            requete = "SELECT * FROM " + selected_table;
            try {
                gestionnaire.initializeStringV2();
            } catch (SQLException ex) {
                Logger.getLogger(Maj.class.getName()).log(Level.SEVERE, null, ex);
            }

            //gestionnaire.setHumanRequest(sql_request);
            gestionnaire.setHumanRequest(requete);
            //entreeUtilisateur.setEditable(false);
            //pan.remove(instructionsMaj);
            //valider.setEnabled(false);
            try {
                //pan.removeAll();
                /*JPanel three_buttons = new JPanel();
                add(three_buttons, "South");
                add(pan, "North");
                JButton ajouter = new JButton("Ajouter un champ");
                JButton supprimer = new JButton("Supprimer la ligne");*/

                //ArrayList<String> resultat = gestionnaire.resultatRequete(sql_request);
                //ArrayList<String> colonne = gestionnaire.getNomColonne(sql_request);
                
                ArrayList<String> resultat = gestionnaire.resultatRequete(requete);
                ArrayList<String> colonne = gestionnaire.getNomsColonne(requete);
                
                String[] table_columns = new String[colonne.size() + 1];
                String[][] data_table = new String[resultat.size() / colonne.size()][colonne.size() + 1];

                table_columns[0] = "#";
                for (int i = 1; i < colonne.size() + 1; ++i) {
                    table_columns[i] = colonne.get(i - 1);
                }

                //onscreen = new JTable(new DefaultTableModel(data_table, table_columns));
                for (int i = 0; i < resultat.size() / colonne.size(); ++i) {
                    data_table[i][0] = Integer.toString(i + 1);
                    for (int j = 1; j < colonne.size() + 1; ++j) {
                        data_table[i][j] = resultat.get((i * colonne.size()) + (j - 1));
                    }
                }

                final JTable onscreen = new JTable(new DefaultTableModel(data_table, table_columns));

                /*three_buttons.add(ajouter, "WEST");
                three_buttons.add(supprimer, "EAST");
                pan.add(onscreen.getTableHeader(), BorderLayout.PAGE_START);
                pan.add(onscreen, BorderLayout.CENTER);*/

                /*JScrollPane scrollPane = new JScrollPane(onscreen);
                pan.add(scrollPane);

                ajouter.addActionListener((ActionEvent d) -> {
                    try {
                        DefaultTableModel model = (DefaultTableModel) onscreen.getModel();
                        String[] new_Row = new String[colonne.size()];
                        for (int i = 0; i < colonne.size(); ++i) {
                            new_Row[i] = " ";
                        }
                        new_Row[0] = Integer.toString(model.getRowCount() + 1);
                        model.addRow(new_Row);
                        String insertIntoStatement = "INSERT INTO " + selected_table;
                        String valuesStatement = " VALUES(";
                        for (int i = 1; i < colonne.size() + 1; ++i) {
                            valuesStatement += "0";
                            if (i != colonne.size()) {
                                valuesStatement += ",";
                            } else {
                                valuesStatement += ")";
                            }
                        }
                        //JOptionPane.showMessageDialog(this, insertIntoStatement + valuesStatement);
                        gestionnaire.miseajour(insertIntoStatement + valuesStatement);
                    } catch (SQLException ex) {
                        Logger.getLogger(Maj.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });

                supprimer.addActionListener((ActionEvent d) -> {
                    try {
                        int row = onscreen.getSelectedRow();
                        String[] condition = new String[colonne.size()];
                        for (int i = 1; i < colonne.size() + 1; ++i) {
                            condition[i - 1] = (String) onscreen.getValueAt(row, i);
                        }
                        DefaultTableModel model = (DefaultTableModel) onscreen.getModel();
                        model.removeRow(row);
                        for (int i = 0; i < onscreen.getRowCount(); ++i) {
                            onscreen.setValueAt(Integer.toString(i + 1), i, 0);
                        }
                        String deleteStatement = "DELETE FROM " + selected_table;
                        String whereStatement = " WHERE ";
                        for (int i = 1; i < colonne.size() + 1; ++i) {
                            whereStatement += table_columns[i] + "='" + condition[i - 1] + "'";
                            if (i != colonne.size()) {
                                whereStatement += " AND ";
                            }
                        }

                        gestionnaire.miseajour(deleteStatement + whereStatement);
                    } catch (SQLException ex) {
                        Logger.getLogger(Maj.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });*/

                //gestionnaire.miseajour(sql_request);<
                try {
                    TableMaj table_maj = new TableMaj(onscreen, selected_table, colonne, gestionnaire);
                    table_maj.setVisible(true);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Recherche.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (SQLException ex) {

                //entreeUtilisateur.setText("Ceci n'est pas une table de la BDD! Veuillez réessayer!");
                Logger.getLogger(Maj.class.getName()).log(Level.SEVERE, null, ex);
            }

            //revalidate();
        }
    }
}
