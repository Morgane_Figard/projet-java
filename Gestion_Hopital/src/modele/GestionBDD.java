/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;
import java.util.Scanner;
import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
/**
 *
 * @author Nicolas Lelouche
 */
public class GestionBDD {

    private Connexion connectionBDDECE;
    private Connexion connectionBDDlocale;
    StringV2 human_request;
    
    public GestionBDD() throws SQLException, ClassNotFoundException
    {
        connectionBDDlocale = new Connexion("hopital","root","1234");
        //connectionBDDECE = new Connexion();
        human_request = new StringV2("");
    }
    
    public void initializeStringV2() throws SQLException
    {
        //ArrayList<String> list = connectionBDDECE.remplirChampsRequete("SHOW TABLES");
        ArrayList<String> list = connectionBDDlocale.remplirChampsRequete("SHOW TABLES");
        human_request.table_names = list;
    }
 
    public void setHumanRequest(String s)
    {
        human_request.setString(s);
    }
    
    public ArrayList resultatRequete(String requete) throws SQLException
    {
        //ArrayList<String> list = connectionBDDECE.remplirChampsRequete(requete);
        ArrayList<String> list = connectionBDDlocale.remplirChampsRequete(requete);
        return list;
    }
    
     public void miseajour(String requete) throws SQLException
    {
        //connectionBDDECE.executeUpdate(requete);
        connectionBDDlocale.executeUpdate(requete);
    }
     
     public ArrayList getNomsColonne(String requete) throws SQLException
    {
        return connectionBDDlocale.remplirChampsTable(requete);
    }
    
    public String getString()
    {
        return human_request.getString();
    }
    /**
     * @throws java.io.FileNotFoundException
     * @throws java.sql.SQLException
     */
    
    public void parseHumanRequest() throws IOException, SQLException
    {
        
        initializeStringV2();
        human_request.removeStopwords();
        human_request.parseKeywords();
        human_request.checkPlural();
        human_request.parseSQLRequest();
        
    }

}
