/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Hashtable;
import javax.swing.*;

/**
 *
 * @author Nicolas Lelouche
 */
public class StringV2 {

    private String str;
    public ArrayList<String> parsed_string, table_names, tablesUsed, fieldsUsed;

    public StringV2(String str) {
        this.str = str;
        parsed_string = new ArrayList();
        table_names = new ArrayList<>();
        tablesUsed = new ArrayList<>();
        fieldsUsed = new ArrayList<>();
    }

    public void setString(String new_string) {
        this.str = new_string;
        str = str.replaceAll(",", " ");
        str = str.trim();
    }

    public String getString() {
        return this.str;
    }

    public String getStringIndex(int index) {
        return this.parsed_string.get(index);
    }

    public String[] toWords() {
        String[] words = str.split("\\s+");
        for (int i = 0; i < words.length; i++) {
            words[i] = words[i].replaceAll("[^\\w]", "");
        }
        return words;
    }

    /**
     *
     * @param input
     * @param tobeRemoved
     * @return the array of strings with the removed elements
     */
    public static String[] removeElements(String[] input, ArrayList tobeRemoved) {
        List<String> list = new ArrayList<>(Arrays.asList(input));
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < tobeRemoved.size(); j++) {
                if (list.get(i).equals(tobeRemoved.get(j))) {
                    list.remove(i);
                }
            }
        }
        return list.toArray(new String[0]);
    }

    public void removeStopwords() throws FileNotFoundException, IOException {
        String[] words = this.toWords();
        String buffer = new String();
        ArrayList<String> stopwords = new ArrayList<>();
        BufferedReader stopwords_file = new BufferedReader(new FileReader("STOPWORDS/stopwords.txt"));
        do {
            buffer = stopwords_file.readLine();
            stopwords.add(buffer);
        } while (buffer != null);
        for (int i = 0; i < words.length; ++i) {
            for (int j = 0; j < stopwords.size(); j++) {
                if (words[i].equals(stopwords.get(j))) {
                    words = StringV2.removeElements(words, stopwords);
                }
            }
            words[i] = words[i].trim();
            parsed_string.add(words[i]);
        }
    }

    public void checkPlural() {
        char[] new_char;
        for (int i = 0; i < parsed_string.size(); ++i) {
            new_char = new char[parsed_string.get(i).length() - 1];
            if (parsed_string.get(i).endsWith("s")) {
                for (int j = 0; j < parsed_string.get(i).length() - 1; ++j) {
                    new_char[j] = parsed_string.get(i).charAt(j);
                }
                parsed_string.add(i, new String(new_char));
                parsed_string.remove(i + 1);
                parsed_string.add(i, parsed_string.get(i).trim());
                parsed_string.remove(i + 1);
            }
        }
    }

    public void parseKeywords() {
        Hashtable newHash = new Hashtable();
        newHash.put("affilie", " ");
        newHash.put("travaillant", "employe");
        newHash.put("donner", " ");
        newHash.put("medecin", "docteur");
        newHash.put("hospitalise", "hospitalisation");
        newHash.put("hopital", "service");
        for (int i = 0; i < parsed_string.size(); ++i) {
            if (newHash.containsKey(parsed_string.get(i))) {
                String set = parsed_string.set(i, (String) newHash.get(parsed_string.get(i)));
            }
        }
    }

    public String parseSQLRequest() {
        ArrayList<String> selectSQL = new ArrayList<>(), fromSQL = new ArrayList<>(),
                whereSQL = new ArrayList<>();
        String[] words;
        words = toWords();
        int last_table = 0;
        if (words[0].equals("SELECT") || words[0].equals("Select") || words[0].equals("select")) {
            return str;
        } else {
            str = "SELECT ";
            int i = 0;
            boolean tableNameFound = false;
            for (i = 0; i < parsed_string.size(); ++i) {
                tableNameFound = false;
                for (int j = 0; j < table_names.size(); ++j) {
                    if (parsed_string.get(i).equals(table_names.get(j))) {
                        tableNameFound = true;
                    }
                }
                if (!tableNameFound) {
                    str += parsed_string.get(i) + ",";
                } else {
                    break;
                }

            }
            str = str.substring(0, str.length() - 1);
            str += " FROM ";
            int nb_table = 0;
            for (i = i; i < parsed_string.size(); ++i) {
                for (int j = 0; j < table_names.size(); ++j) {
                    if (parsed_string.get(i).equals(table_names.get(j))) {
                        str += parsed_string.get(i) + ",";
                        last_table = i;
                        nb_table++;
                    }
                }
            }
            str = str.substring(0, str.length() - 1);
            str += " WHERE " + parsed_string.get(last_table + 1) + "='" + parsed_string.get(last_table + 2) + "'";
            if (nb_table > 1) {
                str += " AND " + parsed_string.get(last_table) + ".numero=" + parsed_string.get(last_table - 1) + ".numero ";
            }
            if (last_table + 3 < parsed_string.size()) {
                if (parsed_string.get(last_table + 3).equals("trier")) {
                    str += " ORDER BY " + parsed_string.get(parsed_string.size() - 1);
                }
            }
        }
        return str;

    }

    private ArrayList parseSelect(String select) {
        ArrayList<String> ret = new ArrayList<>();
        return ret;
    }

    private ArrayList parseFrom(ArrayList<String> from) {
        return from;
    }

    private String parseWhere() {
        String s = new String();
        return s;
    }
}
